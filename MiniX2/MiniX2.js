
function setup() {
// Størrelsen på canvas
 createCanvas(windowWidth, windowHeight)

}

function draw() {
  background(255,255,204);

//Regnbue smiley
noStroke();

//rød 
fill(255,0,0);
ellipse(500,400,290);

//orange
fill(255,102,0);
ellipse(500,400,260);

//gul
fill(255,255,0);
ellipse(500,400,230);

//grøn
fill(51,204,0);
ellipse(500,400,200);

//blå
fill(51,102,255);
ellipse(500,400,170);

//lilla
fill(51,0,102);
ellipse(500,400,140);

//rød 
fill(255,0,0);
ellipse(500,400,110);

//orange
fill(255,102,0);
ellipse(500,400,80);

//gul
fill(255,255,0);
ellipse(500,400,50);

//grøn
fill(51,204,0);
ellipse(500,400,20);

//Øjne på regnbue
noStroke();
fill(255);
circle(530,380,50);
circle(470,380,50);

//Sorte Propiller 
fill(0);
circle(530,390,30);
circle(470,390,30);

//Hvid i øjne
fill(255,255,255);
circle(530,395,18);
circle(470,395,18);

//Mund1 
fill(255);
arc(500,450,100,80,0,PI);

//Mund2 
fill(255,153,153);
arc(500,460,40,mouseY,0,PI);

textSize(20);
fill(0);
text('Move the tongue & have fun',370, 230);

//BRUN SMILEY 
fill(102,51,0);
ellipse(1000,400,290);

//Øjene på brun
fill(255);
circle(1030,380,50);
circle(970,380,50);

//Sorte prikker på brun 
fill(0);
circle(1030,380,30);
circle(970,380,30);

//Hvid i øjne på brun 
fill(255);
circle(1030,368,18);
circle(970,368,18);

//Mund på brun 
fill(255,204,204);
arc(1000,440,100,80,0,PI);
fill(204,0,51);
arc(1000,445,80,60,0,PI);

//Hat på brun 
fill(255,40,40);
ellipse(1000,295,250,30);
ellipse(1000,265,300,75);
ellipse(1000,240,8,75);

//Overskæg på brun 
fill(0);
arc(1000,426,130,10,0,PI);

textSize(20);
text('Press to change', 940, 600);
text('if you dare', 970, 620);



// For at lave to emojis oven på hinadnen bruger jeg ifmouseispressed statement og sætter min nye emoji ind i det statemnet. Det vil sige at hvis mouseispressed skal det skifte. 

if(mouseIsPressed === true) {

//Fjern hat på gul emoji. Jeg har sat en stroke på, da der kommer en meget smal kant fra hatten hvis ikke dette er sat på. 
fill(255,255,204);
stroke(255,255,204);
ellipse(1000,295,250,30);
ellipse(1000,265,300,75);
ellipse(1000,240,8,75);

//Gul emoji 
  fill(255, 255, 0); 
  ellipse(1000,400,290);

// Øje 1 
    fill(255);
    circle(1030,380,50);
    fill(0);
    circle(1030,380,30);
    fill(255);
    circle(1030,368,18);
  
//Øje 2 
  fill(0);
  arc(950,380,40,5,0,PI);

//Mund
  fill(255);
  arc(1000,440,100,80,0,PI);
  fill(204,0,51);
  arc(1000,460,40,90,0,PI);


}
}

