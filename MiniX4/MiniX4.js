

function setup() {
createCanvas(windowWidth, windowHeight);

knap = createButton("DEAR DATA");
knap.position(650,390); 
knap.size (200,100);
knap.style("background-color","#0000FF")
knap.style("color","#ffffff")


//knap 1
let knap1 = [];
let x=0;
let y=5;

for (let i=0; i < 100; i++){
     knap1[i] = createButton("");
     knap1[i].position(x,y);
     knap1[i].style("background-color","#FFA500");
     knap1 [i].size(10,100);
     x += 20; //afstanden fra midten af min knap til midten af den næste knap
     
knap1[i].mouseOut(resetFarve1);
knap1[i].mousePressed(skifteFarve1);
      
function resetFarve1(){ 
knap1[i].style("background-color","#7EC0EE");
}

function skifteFarve1(){ 
knap1[i].style("background-color","#7EC0EE");
}   
}

//knap 2 
let knap2 = [];
let x1=0;
let y1=110;

for (let i=0; i < 100; i++){
     knap2[i] = createButton("");
     knap2[i].position(x1,y1);
     knap2[i].style("background-color","#EED2EE");
     knap2[i].size(10,100);
     x1 += 20; //afstanden fra midten af min knap til midten af den næste knap
     
knap2[i].mouseOut(resetFarve1);
knap2[i].mousePressed(skifteFarve1);
      
function resetFarve1(){ 
knap2[i].style("background-color","#FF6A6A");       
}

function skifteFarve1(){ 
knap2[i].style("background-color","#FF6A6A");
}
}

//Knap 12
let knap12 = [];
let x11=0;
let y11=110;

for (let i=0; i < 100; i++){
     knap12[i] = createButton("");
     knap12[i].position(x11,y11);
     knap12[i].style("background-color","#7EC0EE");
     knap12[i].size(10,100);
     x11 += 20; //afstanden fra midten af min knap til midten af den næste knap
     
knap12[i].mouseOut(resetFarve1);
knap12[i].mousePressed(skifteFarve1);
      
function resetFarve1(){ 
knap12[i].style("background-color","#EED2EE");
}

function skifteFarve1(){ 
knap12[i].style("background-color","#EED2EE");
}
}




//Knap 3 
let knap3 = [];
let x2=-0;
let y2=220;

for (let i=0; i < 100; i++){
     knap3[i] = createButton("");
     knap3[i].position(x2,y2);
     knap3[i].style("background-color","#BDFCC9");
     knap3[i].size(140,20);
     x2 += 150; //afstanden fra midten af min knap til midten af den næste knap
     
knap3[i].mousePressed(skifteFarve1);
      
function resetFarve1(){ 
knap3[i].style("background-color","#00BFFF");       
}

function skifteFarve1(){ 
knap3[i].style("background-color","#00BFFF");
}
}

//Knap 4 
let knap4 = [];
let x3=-0;
let y3=250;

for (let i=0; i < 100; i++){
     knap4[i] = createButton("");
     knap4[i].position(x3,y3);
     knap4[i].style("background-color","#BDFCC9");
     knap4[i].size(140,20);
     x3 += 150; //afstanden fra midten af min knap til midten af den næste knap
     
knap4[i].mousePressed(skifteFarve1);
      
function resetFarve1(){ 
knap4[i].style("background-color","#00BFFF");
}

function skifteFarve1(){ 
knap4[i].style("background-color","#00BFFF");
}
}

//Knap 5
let knap5 = [];
let x4=-0;
let y4=595;

for (let i=0; i < 100; i++){
     knap5[i] = createButton("");
     knap5[i].position(x4,y4);
     knap5[i].style("background-color","#BDFCC9");
     knap5[i].size(140,20);
     x4 += 150; //afstanden fra midten af min knap til midten af den næste knap
     
knap5[i].mousePressed(skifteFarve1);
      
function resetFarve1(){ 
knap5[i].style("background-color","#00BFFF");
}

function skifteFarve1(){ 
knap5[i].style("background-color","#00BFFF");
}
}

//Knap 6
let knap6 = [];
let x5=-0;
let y5=625;

for (let i=0; i < 100; i++){
     knap6[i] = createButton("");
     knap6[i].position(x5,y5);
     knap6[i].style("background-color","#BDFCC9");
     knap6[i].size(140,20);
     x5 += 150; //afstanden fra midten af min knap til midten af den næste knap
     
knap6[i].mousePressed(skifteFarve1);
      
function resetFarve1(){ 
knap6[i].style("background-color","#00BFFF");
}

function skifteFarve1(){ 
knap6[i].style("background-color","#00BFFF");
}
}

//Knap 7
let knap7 = [];
let x6=0;
let y6=655;

for (let i=0; i < 100; i++){
     knap7[i] = createButton("");
     knap7[i].position(x6,y6);
     knap7[i].style("background-color","#FF6A6A");
     knap7[i].size(10,100);
     x6 += 20; //afstanden fra midten af min knap til midten af den næste knap
     
knap7[i].mouseOut(resetFarve1);
knap7[i].mousePressed(skifteFarve1);
      
function resetFarve1(){ 
knap7[i].style("background-color","#FFA500");
}

function skifteFarve1(){ 
knap7[i].style("background-color","#FFA500");
}
     
}
//Knap 8 
let knap8 = [];
let x7=0;
let y7=760;

for (let i=0; i < 100; i++){
     knap8[i] = createButton("");
     knap8[i].position(x7,y7);
     knap8[i].style("background-color","#7EC0EE");
     knap8[i].size(10,100);
     x7 += 20; //afstanden fra midten af min knap til midten af den næste knap
     
knap8[i].mouseOut(resetFarve1);
knap8[i].mousePressed(skifteFarve1);
      
function resetFarve1(){ 
knap8[i].style("background-color","#EED2EE");
}

function skifteFarve1(){ 
knap8[i].style("background-color","#EED2EE");
}
}

//Knap 9
let knap9 = [];
let x8=0;
let y8=820;

for (let i=0; i < 100; i++){
     knap9[i] = createButton("");
     knap9[i].position(x8,y8);
     knap9[i].style("background-color","#7EC0EE");
     knap9[i].size(10,100);
     x8 += 20; //afstanden fra midten af min knap til midten af den næste knap
     
knap8[i].mouseOut(resetFarve1);
knap8[i].mousePressed(skifteFarve1);
      
function resetFarve1(){ 
knap8[i].style("background-color","#EED2EE");
}

function skifteFarve1(){ 
knap8[i].style("background-color","#EED2EE");
}
}

//Knap 10
let knap10 = [];
let x9=0;
let y9=520;

for (let i=0; i < 100; i++){
     knap10[i] = createButton("");
     knap10[i].position(x9,y9);
     knap10[i].style("background-color","#FFC0CB");
     knap10[i].size(60,50);
     x9 += 100; //afstanden fra midten af min knap til midten af den næste knap


}

//Knap 11
let knap11 = [];
let x10=0;
let y10=300;

for (let i=0; i < 100; i++){
     knap11[i] = createButton("");
     knap11[i].position(x10,y10);
     knap11[i].style("background-color","#FFC0CB");
     knap11[i].size(60,50);
     x10 += 100; //afstanden fra midten af min knap til midten af den næste knap


}

}

function draw (){
background (255,245,245);

}


function keyPressed(){

     if(keyCode === 32){
         knap.style("transform","rotate(180deg)");
     

     

     
     }
 }



