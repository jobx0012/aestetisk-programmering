
let ninja; 
let baggrund;
let frugtninja;
let frugt = []; 
let point = 0;

let antal = 15; 

function preload(){

  frugtninja = loadImage("frugtninja.png");
  baggrund = loadImage ("background.webp");
  frugtbillede = loadImage("frugtbillede.png");

}

function setup (){
createCanvas(windowWidth,windowHeight);
ninja = new Ninja();

for (let i = 0; i < antal; i++) {
  frugt.push(new Frugt());


}

}

function draw () {
background(255);
image(baggrund,0,0,width,height);
ninja.show();
checkCollision();
spawnFrugt();
fill(255);
text("Sliced fruit : "+point, width/2, 100)
textAlign(CENTER); 

}

function spawnFrugt() {
  for (let i = 0; i < frugt.length; i++) {
      frugt[i].move();
      frugt[i].show();
    
  }
}



function checkCollision() {
  for (let i = 0; i < frugt.length; i++) {

  if (mouseX > frugt[i].posX - 5 && mouseX < frugt[i].posX + 5) {

          frugt.splice(i, 1);
          point++ // betyder at man lægger 1 til variablen point
          frugt.push(new Frugt());
         
          }
      
      }
  }


