# ReadMe MiniX6 

Link til mit kørende program: https://sofieamaliem.gitlab.io/aestetisk-programmering/MiniX6/index.html

Link til min kode: https://gitlab.com/sofieamaliem/aestetisk-programmering/-/blob/main/MiniX6/frugtninjaspil.js 

Link til min frugt-klasse: https://gitlab.com/sofieamaliem/aestetisk-programmering/-/blob/main/MiniX6/Frugt.js 

Link til min ninja-klasse: https://gitlab.com/sofieamaliem/aestetisk-programmering/-/blob/main/MiniX6/Ninja.js 

![](/MiniX6/Skærmbillede_fruitninja.png)

## Introduktion 

Denne uges MiniX opgave har jeg valgt at lave en rekonstruktion af mobilspillet 'Fruit ninja' hvor at man istedet for at slice frugten, så er det tiltænkt at når musen 'fruit ninjaen' rammer en frugt får man et point. Jeg har i min MiniX opgave haft stor fokus på at implementere de rigtige klasser med de rigtige objekter og properties. Jeg har derfor, modsat mine andre MiniX forsøgt at lave en mere simpel kode, da forståelse bag skulle på plads først ;) 

## Min kode 

Som nævnt i min korte introduktion så er min kode forholdsvis simpel. Jeg vil forklare først min klasser og dermed min hoved sketch hvor mit 'fruit ninja' spil ligger inde i. 

Jeg har først lavede en klasse til min Ninja. Hernøst tilføjede jeg først min consttuctor, som er her hvor min Ninja bliver beskrævet afhæning af hvilke properties den skal have. Jeg har blot tilføjede min x og y position til at starte med ved at sige: 

```
class Ninja {
    constructor (){
        this.posX = width/2;
        this.posY = height/2;

    
}
```
Det vil altså sige at min ninja den er placeret i midten da jeg har dividerede vidden og højden med 2. Dernæst for at få min Ninja til at følge min mus, bruger jeg show funktionen. Da jeg har med et billede at gøre, som skal 'erstatte' min mus, så bruger jeg oush og på så min imageMode(CENTER) ikke påvirker de andre billeder jeg har i mit spil. 

```
show(){
    push()
    imageMode(CENTER);
    image(frugtninja, constrain(mouseX,0,width),constrain(mouseY,0,height),350,300);
    pop()

}
```

Min Ninja-klasse er forholdvis simpel, så vil lige også forklare min Frugt-klasse. I min frugtklasse starter jeg ud, med samme fremgangsmåde som i min Ninja klasse. Jeg lavede en constructor hvor jeg tilføjde this.posX, this.posY og this. speed, da min frugt skal have en bevægelse. 

```
class Frugt {
    constructor (){
        this.posX = random (0,width);
        this.posY = random (0,100);
        this.speed = random(-0.5,2);
    
}
```

Mine frugter skal altså starte ved 0 og på x-aksen fortsætter den bare ud til vidden af canvas i en random sekvens. Har ligeledes sat min this.speed til at være random men at den skal være random i -0.5-2. 

For dernæst at tilføje movement til min kode, har jeg brugt funktionenn `move() ` som er det gør gør, at jeg kan flytte mine objekter på skærmen. Det vil sige at mit objekts position på y-aksen øges med den hastighed jeg har sat den til i min constructor. Hvis objektets position går ud over højden på mit canvas, har jeg sat den til en tilfølgidg værdi mellem -100 og -50. 
```
move(){
    this.posY += this.speed;
    if (this.posY > height+50){
        this.posY = random(-100, -50);
    }

}
```

For dernæst at kunne vise mit billede har jeg brugt show() funktionen ved at sige: 

```
show(){
    image(frugtbillede,this.posX, this.posY ,50,50);

}
```
Jeg har tilføjde navnet på mit billede, dens x og y position jeg gav den oppe i min constructor og tilføjde størrelsen på mit billede tilsidst som er (.....50,50);

Jeg vil ikke gå slavisk igennem min kode, men istedet vælge de punkter ud der passer til de forskellige klasser. 

Nu når min klasser er færdige vil jeg nu bevæge mig videre til mit Frugtninjaspil hvor jeg bruge de funktioner jeg har i mine klasser, 
Jeg starter naturligvis ud med at lave nogle globlae variabler som jeg skal bruge længere ned i min kode. Jeg har tilføjede variabler til både mine klasser, min baggrund (da det også er et billede), point og det antal frugter jeg ønsker på mit canvas. 

```
let ninja; 
let baggrund;
let frugtninja;
let frugt = []; 
let point = 0;
let antal = 15; 
```

Dertil gør jeg brug af function preload () da jeg skal have loadede mine billeder til mit canvas. Function preload() bruges til at ' Handle asynchronous loading of external files in a blocking way. If a preload function is defined, setup() will wait until any load calls within have finished. ' (citat p5.js Referance)

I min function setup() laver jeg selvfølgelig et canvas der passer til min skærm: 

createCanvas(windowWidth,windowHeight);

og dernæst tildeler jeg min ninjaklasse variablen ninja. 

ninja = new Ninja(); (referer til linja 33 i min kode);

For dernæst at få min ninja vist på min canvas skriver jeg i min function draw() 

ninjs.show(); (referer til linja 33 i min kode);

Min ninja er nu på plads og følger min mus rundt. Jeg laver dernæst mine fuktioner til min frugt.

I function setup() har jeg lavet et forloop, hvor jeg har implementerede et array da jeg i min globale variabel har sat min frugt til at være lig et array (referer til linje 5 i min kode); 

```
for (let i = 0; i < antal; i++) {
  frugt.push(new Frugt());

}
```

I mit for-loop har jeg sat at når i er lig 0, og i er mindre end mit antal som er 15 så skal funktionen kører i loop. Det vil sige jeg bruger mit for-loop til at kreere nye frugt-objekter hele tiden og tilføjet dette til mit frugt-array. 

For dernæst at få mine frugter til at blive vist og bevæge sig, så har jeg sat den til at hvert element i mit frugt-array løber igennem og kalder på min move() og show()-funktioner som gør at der kommer bevægelse på mit canvas. 

```
function spawnFrugt() {
  for (let i = 0; i < frugt.length; i++) {
      frugt[i].move();
      frugt[i].show();
  }
```
Jeg tilføjer ligeledes min function spawnFrugt() oppe i min function draw() så den bliver vist igen og igen. 

For at få point i mit spil, skal ninja'en blot ramme en vandmelon. Dette har jeg gjort ved først at sætte et for-loop ind og dernæst lavede et if-statement hvor at jeg får den til at tjekke om ninjaens posX er inden for frugtens posX. Her har jeg sat et vilkårligt interval på 10. Når den så rammer frugten skal min point-tæller så stige med et point. 

```
function checkCollision() {
  for (let i = 0; i < frugt.length; i++) {
    
  if (mouseX > frugt[i].posX - 10 && mouseX < frugt[i].posX + 10) {

          frugt.splice(i, 1);
          point++ // betyder at man lægger 1 til variablen point
          frugt.push(new Frugt());
         
          }
      
      }
  }
```



Kort vil jeg lige afslutte med, at forklare hvordan jeg har implementerede min text. Jeg har i min `function draw()` tilføjde følgende:

```
fill(255);
text("Sliced fruit : "+point, width/2, 100)
textAlign(CENTER); 
```


Det vil altså sige at jeg har valgt min tekst skal være hvid, hvad der skal stå og tilføjde en point-tæller som jeg også bruger nede i min function `checkCollision() `og brugt `textAlign(CENTER)` for at placere min text i midten. 

## Tanken bag  

Mit spil går ud på at min mus, som er en ninja, skal slice frugterne der falder ned fra toppen. Mine frugter er sat til at falde random ned fra toppen af og Ninja skal så slice så mange frugter som muligt. Jeg ville gerne lave et spil, der kom så tæt på det orgianle spil som muligt, og valgte derfor at bruge den samme grafiske elementer. I forholdt til de andre miniX opgaver vi har lavet, synes jeg denne opgave var rigtig svær på refleksionsdelen. Jeg føler lidt at den måde jeg har valgt at lave et spil på, gør at der ikke er mange muligheder til reflektioneren og jeg på den måde har begrænset mig selv lidt. Jeg tænker derfor også denne kunne være opslag at bruge i næste uges miniX opgave fordi den helt klart kunne få implementerede nogle federe funktioner og noget mere reflektion. 

Min MiniX opgave indeholder de her classes som er indelt i class ninja () og class Frugt () der hver er et objekt i mit spil og har hver sin funktion. Som vi læste i teksten i Aesthetic programming, A handbook of Software Studies så handler emnet og generelt obejekt orienteret programmering meget om objektet som et nøglebegreb hvor det er en ting med egenskab som jeg synes er en ret interessant måde at se et programmeringsobjekt på. Jeg havde ikke selv set programmering på den måde, og synes dermed det var interessant at dykke lidt mere ned i dette. De stiller tankegangen op omkring subjekt og objekt i programmering på en god måde i teksten: 

> _Put simply, and following philosophical conventions, a subject is an observer (we might say programmer) and an object is a thing outside of this, something observed (say a program).........in line with people who think we need to put more emphasis on non-human things so we can better understand how objects exist and interact, both with other objects, but also with subjects. (pp. 145)_


En meget simpel og basal måde at se programmering på hvor subjektet og obejekt skilles ad, og den vinekl har jeg forsøgt mig at arbejde lidt ud fra i denne miniX, det med at forstå objektet på en anden måde. 

Så er der naturligvis også det begreb vi kalder 'abstraction' hvor vi i teksten læste at: 

> _Abstraction is one of the key concepts of “Object-Oriented Programming” (OOP), a paradigm of programming in which programs are organized around data, or objects, rather than functions and logic. 1 The main goal is to handle an object’s complexity by abstracting certain details and presenting a concrete model._

Det med abstraction i programmering og at det er en nødvendighed i OOP at organisere i class og properties giver rent overordnede meneing i mit hovedet, men rent logisk i computeren forstår jeg ikke helt hvordan denne del fungere. Det er en interesant måde at arbejde med programmering på og tror helt klar jeg vil tage det med videre, men det er som om der ikke kommer så mange refleksioner op i mit hovedet. Tror måske det er fordi det er logik og i min verden er der ikke mange reflektioner omkring logik og fakta. 

Vil blot bare lige afslutte af med et citat fra introen til emnet fra grundbogen som jeg synes er et ret godt citat: 

> _Beatrice Fazi and Matthew Fuller have outlined the wider significance of this and the relations between concrete and abstracted computation: “Computation not only abstracts from the world in order to model and represent it; through such abstractions, it also partakes in it.” (pp. 145)_


## Referance

https://p5js.org/reference/

Soon, Winnie & Cox, Geoff - Aesthetic programming, A handbook of Software Studies

Fruit Ninja, Halfbrick Studios 
