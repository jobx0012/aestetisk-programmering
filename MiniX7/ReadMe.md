## Introduktion 
Jeg har i denne Minix7 valgt at genbesøge min MiniX6 af flere grunde. Jeg synes først at der var mange flere ting jeg kunne ændre, siden det var et emnet jeg ikke forstod så godt. Jeg brugte derfor ikke mine lærte teknikker fra tidligere miniX så meget, men mere den metodiske tilgang til min miniX og begyndte at for at forstå min kode, og se hvor der opstod fejl. 

Derfor har jeg fokusere på at arbejde med de fejl der var i min MiniX6 og tilføje nogle funktioner jeg ikke kunne i MiniX6. Mit fokus har derfor været at tilføje flere frugter til mit spil, hvilket viste sig at være en smule kompliceret fordi computeren havde svært ved de navne jeg havde givet mine frugter og de kunne derfor ikke loades i mit spil. Det har jeg rettet op på, ved at give min class et andet navn fra mit uploadet billede (irriterende lille fejl)
Jeg har ligeledes rettede fejlen, hvor mine frugter før både kørte op og ned på mit canvas hvilket ikke var meningen. _(linje 7 i Frugt.js)_

Alt i alt har jeg valgt at genarbejde min minix6 fordi det var et ret dårligt spil jeg havde lavede både layout mæssigt og funktionsmæssigt. Men det var vigtigst for mig at lave en opgave der fokuserede på forståelsen af objektorienteret programmering og ikke det visuelle eller funktionsdygtige. 

Jeg har i denne opgave lært hvordan objektorienteret programmering fungere og hvor besværlig det egentligt er. Jeg har i denne miniX valgt at fokuseret en del på den teoretiske og litteraturmæssige tilgang til opgaven, da det helt klart er der jeg synes det interessante ligger, men også det der giver mest mening for mig. Jeg tror at den praktiske del af objektorienteret programmering er en anelse forvirrende for mig, da man hele tiden skal være opmærksom på måde ens hovedfil, men også ens classes. Kan dog godt se fordelen i dette, men er mere til den tilgang i programmering vi primært har arbejde med. 

Link til mit spil : https://sofieamaliem.gitlab.io/aestetisk-programmering/MiniX7/index.html 


Link til min hovedfil: https://gitlab.com/sofieamaliem/aestetisk-programmering/-/blob/main/MiniX7/frugtninjaspil.js 

![](/MiniX7/Skærmbillede_2023-04-11_kl._21.53.37.png)

## Min kode 

Jeg vil starte med at forklare de classes jeg har lavet som hver indeholder de objekter jeg bruger i min frugt-ninja-spil. Jeg har først lavede en `class Frugt` hvor jeg i den klasse har lavet en construtor til min `frugt-klasse`. I denne `contructor` har jeg definerede mine attributter det vil sige: `this.posX, this.posY, this.speed, this.sizex og this.sizeY`: 
```
class Frugt {
    constructor() {
        this.posX = random(0, width);
        this.posY = random(0, 100);
        this.speed = random(0.5, 2);
        this.sizeX = 50;
        this.sizeY = 50;

    }
```
Jeg har sat både min` x og y-position` og speed til at være random i et intervaller som for `this.posX er 0` til vidden af canvas, `this.posY er 0` til 100 og tilsidst `this.speed er i et interval 0.5 til 2.` 


Ved at lave denne `class Frugt () ` specificeres strukturen af objekter, attributter og de handlinger objekterne gør. 

Jeg har dernæst lavede en **"class method"** som bruges til at beskrive mine objekters **"behavior"**. I dette tilfælde har jeg sat `.move()` ind, hvor mit objekt skal flytte sig rundt på mit canvas, sammen med min mus. Først øges `this.posY` med objekters hastighed (altså frugten). Hvis objektet(frugten) position er større end højden på mit canvas plus 50 pixels, bliver positionen nulstillede tilfældei et interval mellem -100 og -50. 

```
  move() {
        this.posY += this.speed;
        if (this.posY > height + 50) {
            this.posY = random(-100, -50);
        }

    }
```

Til sidst i min `class Frugt () ` har jeg lavede en **"class method"** som viser mit objekt. I denne **show-method** har jeg sat mit billede, som jeg har kaldt frugtbillede, ind, samt tilføjede mine attributter ind som er min x og y-positioner. 

```
 show() {
        image(frugtbillede, this.posX, this.posY, 50, 50);
    }
```
Jeg har gjort dette ved begge mine frugter, og i min `class Ninja`, har jeg igen gjort brug af **class method show()**, som bruges til at kalde på mit ninjabillede. Her har jeg sat mit billede i `push() og pop()`, så `imageMode(CENTER)` ikke påvirker min baggrund. Jeg har sat billedet placering til at være 'begrænset' til skærmens størrelse med musens x og y-kordinater som referencepunkter. Størrelsen på billedet har jeg bestemt ved mine variabler `sizeX` og `sizeY`. 


```
class Ninja {
    constructor() {
        this.posX = width / 2;
        this.posY = height / 2;
        this.sizeX = 350;
        this.sizeY = 300;

    }

   show() {
        push()
        imageMode(CENTER);
        image(frugtninja, constrain(mouseX, 0, width), constrain(mouseY, 0, height), this.sizeX, this.sizeY);
        pop()

    }
```

I min hovedfil har jeg først lavede en række **globale variabler**, der er nødvendige for at mit program fortsår at de **classes** og **class-methods** jeg har lavet, skal gælde her også. 

```
let ninja;
let baggrund;
let frugtninja;
let frugt = [];
let granatable = [];

let point = 0;
let antal = 7;
```

Jeg har lavet en **variabel for hvert objekt** jeg har, og dermed sat hhv. Frugt og granatable til et array, det gør det muligt at gemme flere dataværdier på en gang. Jeg har ligeledes lavede en variable til mine point der tæller i højre hjørne og antallet af frugter der kommer på skærmen. 

For at få mine billeder ind har jeg brugt funktionen `function preload()` 

```
function preload() {

  frugtninja = loadImage("frugtninja.png");
  baggrund = loadImage("background.webp");
  frugtbillede = loadImage("frugtbillede.png");
  granatablebillede = loadImage("granatablebillede.png");

}
```


I min function setup() har 'kaldt' på min ninja ved at skrive `ninja = new Ninja();`
  
```
function setup() {
  for (let i = 0; i < antal; i++) { 
    frugt.push(new Frugt());

  }
  for (let i = 0; i < antal; i++) {
    granatable.push(new Granatable());
  }
```
Jeg har for hver **frugt-objekt **lavet et **for-loop** der indeholder variablen i til at være lig 0. Loopet fortsætter dermed indtil variablen i ikke længere er mindre end variablen antal som jeg har sat til at være 7. Hver gang mit **for-loop** kører bliver der skubbede _(referer til linje 25 i frugtninjaspil.js)_ et nyt objekt ind i **arrayet "frugt" eller "granatable"** 


I min `function draw()` har jeg først sat en ensfarvet baggrund ind og dernæst tilføjede mit baggrundsbillede ved at sige: `image(baggrund, 0, 0, width, height);` 

Jeg kalder ligeledes på både min `ninja.show();` men også mine lokale **checkCollison-funktioner** og mine **spawn-funktioner** _(jeg vil vende tilbage til denne del)_

```
 ninja.show();

  checkCollision();
  checkCollision1();

  spawnFrugt();
  spawnGranatable();
```
For at tilføje min 'tæller' i højre hjørne, har jeg brugt `fill(), textSize() og textAlign ()`. Har tilføjede den tekst, jeg ønskede og tilføjede værdien af min **globale variable point** samt **placereingen på mit canvas.**

```
fill(255);
textSize(20)
textAlign(RIGHT);
text("Sliced fruit : " + point, width - 50, 75)
```

For at vende tilbage til mine funktioner `spawnFrugt` og `spawnGranatable`. I min funktion `spawnFrugt()` udføres de **class methods** jeg omtalte tidligere, altså `.move()` og `.show()` på hvert objekt i arrayet. For-loopet  indeholder variablen i til at være lig 0. Loopet fortsætter dermed indtil `variablen i` ikke længere er mindre end arrayt `"frugt"` eller `"granatable"`. Hver gang for-loopet 'kører' kaldes funktionerne `.move()` og `.show()` på det aktuelle objekt i arrayet. 

```
function spawnFrugt() { 
  for (let i = 0; i < frugt.length; i++) {
    frugt[i].move();
    frugt[i].show();

  }
}

function spawnGranatable() {
  for (let i = 0; i < granatable.length; i++) {
    granatable[i].move();
    granatable[i].show();

  }

}
```
Tilslut vil jeg forklare min `funktion checkCollision` der er den funktion der **kontrollere sammenstøddet mellem musen og fugterne i arrayet.** Den løber gennem arrys og tjekker om musens x-position er inden for 5 pixels fra frugternes positioner. Hvis der er et sammenstød, bliver frugtobjekterne fjernet fra arrayet, og værdien af variablen øges med 1 og et nyt frugtobjekt tilføjes til arrayet. 

```
function checkCollision() {
  for (let i = 0; i < frugt.length; i++) {

    if (mouseX > frugt[i].posX - 5 && mouseX < frugt[i].posX + 5) {

      frugt.splice(i, 1);
      point++ // betyder at man lægger 1 til variablen point
      frugt.push(new Frugt());
```

Jeg har lavet en `function checkCollision()` til hver af mine frugter: 

```
function checkCollision1() {
  for (let i = 0; i < granatable.length; i++) {
    
    if (mouseX > granatable[i].posX - 5 && mouseX < granatable[i].posX + 5) {

      granatable.splice(i, 1);
      point++ // betyder at man lægger 1 til variablen point
      granatable.push(new Granatable());
    }

  }
}
```


## Tanken bag  

Jeg har lært i denne miniX hvor meget et program kan indeholde, og hvor mange funktioner der kan tilføjes ved blot at bruge classes. Det hænger også lidt sammen med det jeg forstillede mig æstetisk programmering handlede om. Det æstetiske aspekt i det meget kontrollede og organiserede måde at sætte sit program op på. 

Som vi læste i teksten i Aesthetic programming, A handbook of Software Studies så handler emnet og generelt obejekt orienteret programmering meget om objektet som et nøglebegreb hvor det er en ting med egenskab som jeg synes er en ret interessant måde at se et programmeringsobjekt på. Jeg havde ikke selv set programmering på den måde, og synes dermed det var interessant at dykke lidt mere ned i dette. De stiller tankegangen op omkring subjekt og objekt i programmering på en god måde i teksten: 

> _Put simply, and following philosophical conventions, a subject is an observer (we might say programmer) and an object is a thing outside of this, something observed (say a program).........in line with people who think we need to put more emphasis on non-human things so we can better understand how objects exist and interact, both with other objects, but also with subjects. (pp. 145)_

En meget simpel og basal måde at se programmering på hvor subjektet og obejekt skilles ad, og den vinekl har jeg forsøgt mig at arbejde lidt ud fra i denne miniX, det med at forstå objektet på en anden måde. 

Så er der naturligvis også det begreb vi kalder 'abstraction' hvor vi i teksten læste at: 

> _Abstraction is one of the key concepts of “Object-Oriented Programming” (OOP), a paradigm of programming in which programs are organized around data, or objects, rather than functions and logic. 1 The main goal is to handle an object’s complexity by abstracting certain details and presenting a concrete model._

Det med abstraction i programmering og at det er en nødvendighed i OOP at organisere i class og properties giver rent overordnede meneing i mit hovedet, men rent logisk i computeren forstår jeg ikke helt hvordan denne del fungere. Det er en interesant måde at arbejde med programmering på og tror helt klar jeg vil tage det med videre, men det er som om der ikke kommer så mange refleksioner op i mit hovedet. Tror måske det er fordi det er logik og i min verden er der ikke mange reflektioner omkring logik og fakta. 


**Hvad er relationen mellem æstetisk programmering og digital kultur? Hvordan demonstrere dit værk perspektiver i æstetisk programmeing (henvend evt. til forordet i grundbogen)?**

I og med at mit 'værk' skal vise hvad jeg har lært gennem de sidste miniX, synes jeg også at hele forståelse af æstetisk programmeringe er interessant. Vi har ikke kun ændrede forståelse af begrebet æstetisk programmering, men også forståelse for forskellige metoder at programmere på. Netop emnet objektorienteret programmering ligger tæt op af hvad jeg forstod æstetisk programmering var - det æstetiske, organiserede og ordenlige aspekt af noget meget komplekst. Men som vi læste i forordet til vores grundbog, handler æstetrisk programmering om meget mere. At udvide programmeing ud over grænserne, og skabe en bredere forståelse af netop dette. De understreget her vigtigheden i at forstå både læsningen, skrivningen og tænktning med software som et kritisk værktøj. 


## Referance

https://p5js.org/reference/

Soon, Winnie & Cox, Geoff - Aesthetic programming, A handbook of Software Studies

Fruit Ninja, Halfbrick Studios 
