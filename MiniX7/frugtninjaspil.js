
let ninja;
let baggrund;
let frugtninja;
let frugt = [];
let granatable = [];

let point = 0;
let antal = 7;

function preload() {

  frugtninja = loadImage("frugtninja.png");
  baggrund = loadImage("background.webp");
  frugtbillede = loadImage("frugtbillede.png");
  granatablebillede = loadImage("granatablebillede.png");

}

function setup() {
  createCanvas(windowWidth, windowHeight);
  ninja = new Ninja();

  for (let i = 0; i < antal; i++) { 
    frugt.push(new Frugt());

  }
  for (let i = 0; i < antal; i++) {
    granatable.push(new Granatable());
  }

}

function draw() {
  background(255);
  image(baggrund, 0, 0, width, height);

  ninja.show();

  checkCollision();
  checkCollision1();

  spawnFrugt();
  spawnGranatable();

  // tekst der siger hvor mange point min ninja har fået 
  fill(255);
  textSize(20)
  textAlign(RIGHT);
  text("Sliced fruit : " + point, width - 50, 75)


}

function spawnFrugt() { 
  for (let i = 0; i < frugt.length; i++) {
    frugt[i].move();
    frugt[i].show();

  }
}

function spawnGranatable() {
  for (let i = 0; i < granatable.length; i++) {
    granatable[i].move();
    granatable[i].show();

  }

}

function checkCollision() {
  for (let i = 0; i < frugt.length; i++) {

    if (mouseX > frugt[i].posX - 5 && mouseX < frugt[i].posX + 5) {

      frugt.splice(i, 1);
      point++ // betyder at man lægger 1 til variablen point
      frugt.push(new Frugt());



    }

  }
} 

function checkCollision1() {
  for (let i = 0; i < granatable.length; i++) {
    
    if (mouseX > granatable[i].posX - 5 && mouseX < granatable[i].posX + 5) {

      granatable.splice(i, 1);
      point++ // betyder at man lægger 1 til variablen point
      granatable.push(new Granatable());


    }

  }
}



//Jeg tænker at man skal lave et if statement der siger at når ninja har sliced 20 frugter så stiger hastigheden og hvis ninja har misset 10 frugtert så er spillet slut 

