# ReadMe - MiniX5

![](/MiniX5/MiniX5__Skærmbillede.png)

**Link til min kode**: https://gitlab.com/sofieamaliem/aestetisk-programmering/-/blob/main/MiniX5/MiniX5.js

**Link til min generative kode:**
 https://sofieamaliem.gitlab.io/aestetisk-programmering/MiniX5/index.html 

## Introduktion og generativitet 

Jeg har i denne uges MiniX opgave valgt at gribe opgaven lidt anderledes an. Både fordi ugens emne er generativ kode, og vi fik stillet nogle specifikke regler vores kode skulle udfylde, men også fordi jeg synes at generativ kunst er et utroligt spændende perspektiv på denne uges miniX5 opgave. Jeg valgte dermed at forsøge mig med en udfordrende kode samtidig med at den opstiller regler for mit program. Mine regler var at: 

1.	Ved hjælp af for-loops skal der genereres et hvis antal rektangler. 
2.	Når hver af mine rektangler rammer en bestemt værdi, skal deres bevægelse starte forfra igen. 

Måden jeg har lavet min kode på, er at starte med at lave de nødvendige globale variabler, så jeg kunne få frihed til at ændre mine `rect()` i forholdt til deres placering på canvas, størrelsen osv. Jeg har både lavet globale variabler til mine `rect() x og y værdier`, deres højde og brede samt en global variabel til `a` som jeg vil komme ind på længere nede. 

```
let firkant1x 
let firkant1y 

let firkant1brede = 100
let firkant1højde = 100

let a1 = 100
```


Min kode består primært at for-loops og if-statements som er identiske hele vejen ned i min kode. Jeg vil derfor forklare et for-loop og gøre dette mere specifikt. 

Jeg begyndte naturligvis med at vælge min baggrund, tilføjede stroke til mine `rect()` og tilføjede `noFill()`. For at mine `rect()` var centeret tilføjede jeg `rectMode(CENTER) `. 

```
background (255,200,200);
  stroke(220,20,60);
  frameRate();
  strokeWeight(2);
  noFill() 
  rectMode(CENTER);
```

Jeg lavede dernæst mit `for-loop`. Her definerede jeg min `tæller` og satte den `lig 0` og satte min tæller `mindre` `end` `8`. Dette vil sige at jeg generede i mit `for-loop` af jeg skulle have `8 rect()` i min kode og dette skulle gå i loop ved at sige `tæller++`


`for (let tæller = 0; tæller < 8; tæller++){`


Jeg erklærede dernæst en konstant i mit `for-loop` som jeg valgte at kalde `’const afstand’`. Da der er tale om en konstant, kan denne kun eksistere inden for det `for-loop` den er lavet i. Min konstant ændre dermed navn i hvert nyt `for-loop` jeg har lavet. 

`const afstand = 155 -(tæller *30);`

Jeg valgte at sætte min afstand til at være `155-(tæller*30)` da afstanden fra en `rect()` til den næste `rect()` skulle være `155` og for at skabe den præcis samme afstand mellem alle, minusser jeg min afstand med `tælleren*30.` 

For at kunne skabe mange `rect()` lavede jeg først en der var i midten af canvas hvor jeg skiftede den `(x,y,h,b)` ud med mine pre-definerede globale variabler. 

`rect(firkant1x,firkant1y,firkant1brede,firkant1højde);`

For dernæst at få genereret ens firkanter på begge sider af min `rect()` placeret i `CENTER` skulle jeg lave en sætning hvori jeg både fik implementeret min `rect()` og afstanden som jeg erklærede tidligere. Jeg gjorde dermed brug af min tæller igen og gangede med min afstand for at størrelsen på mine `rect()` blev mindre og mindre indtil de ramte nr. 8 som jeg skrev i **linje 84** af min kode. 


  ```
  rect(firkant1x,
    firkant1y-((tæller+1)*afstand),
    firkant1brede-((tæller+1)*30), 
    firkant1højde-((tæller+1)*30));

    rect(firkant1x, 
    firkant1y+((tæller+1)*afstand),
    firkant1brede-((tæller+1)*30), 
    firkant1højde-((tæller+1)*30));
```


Jeg gjorde dette både ved – og + så jeg fik genereret firkanter både over min `rect()` og under min `rect() `
For dernæst af få mine `rect()` til at bevæge sig rundt om sig selv, benyttede jeg `sin(a)`. Jeg har i mine globale variabler definerede a som en variable og sat den til at være lig `100`. 

Jeg brugte en forholdvis simpel kode for at implementere `sin(a)`. Jeg bruger `Math.sin(a)` da det er en matematisk funktion der beregner sinus af `variablen a`, og for at skabe en jævn bevægelsen langs `x-aksen.` 

For at bestemme hvor stor en svingning min `rect()` skal have sætter jeg min variable `a += 0.005`, da jo mindre `a` er desto større udsving laver den. Jeg implementerede ligeledes en random funktion for at mit udsving placerede sig random. 

```
firkant1x = firkant1x + Math.sin(a1);
a1 +=random(0.005)
```

Sidst tilføjede jeg et if-statement der siger at hvis min rect() er større end eller lig med 1500 så skal rect() starte forfra ved 50 pixels. 

```
if(rect >= 1500){
      rect = 50; }
```

## Tanken bag min generative kode  

Generativ kode er for mange mange ting, og jeg kan helt klart se hvorfor netop dette emne er interesant. Der er mange spændende aspekter i emnet som er relevant at komme ind på, da generativitet jo både handler om en blanding af kontrol og randomness. Hvis man skulle skille de to ting ad, så er randomness for mig ofte lig med noget ikke struktureret eller kontrolleret og måske en tendens til at det er for sjusket og dermed ikke hænger særlig godt sammen med begrebet noise hvor naturlighed spiller en stor rolle. Random og naturlighed (noise) er for mig to forskellige ting, men når man taler om en generativ kode eller generativ kunst, som dette ofte går hen og bliver, virker det mere logisk at naturlighed var et hovedmene og ikke randomness. Dette afhænger også meget af hvordan man ser på koden og hvordan man vælger at tilgå den. 

For mig, falder naturligheden måske også lidt fra, i og med at man som kunster eller koder vælger hvordan programemt skal forløbe, selvom man implementere funktioner der er random() eller noise(). Det er derfor interessant at arbejde med at minimere de regler man ofte sætter for sig selv, når man koder eller skaber kunst. Man mister dermed også som 'forfatter' eller 'kunster' også en del kontrol, som kan hænge sammen med netop programmering. Der er mange aspekter i denne kritiske vinkel på generativ kode, da man kan sige at naturligheden spiller en væsentlig rolle, men samtidig vil kontrollen overtage i forbindelse med det reglsæt der bliver sat op i netop en opgave som denne. 

Jeg synes det citat de omtaler i Aesthetic programming, A handbook of Software Studies omfavner godt de reflektsioner der spiller en rolle i netop dette emne: 

_“The idea becomes a machine that makes the art,” as LeWitt explains. Using the programming language Processing, this is taken as an invitation by Casey Reas to render LeWitt’s wall drawings on the basis of their instructions, thereby exploring the parallels of interpretation and process for each of them"...."Nevertheless it is the close connection and overlap that interests him, and underlies the development of Processing as a “software sketchbook” as Reas wanted programming to be as immediate and fluid as drawing." (pp.124)_

I min kode har jeg derfor, valgt at fokusere meget på det med at min kode er random, men den er kontrolleret random. Det vil sige at jeg har sat retningslinjerne for hvordan den skal udforme sig, og bevæge sig, men den bevæge sig helt efter hvordan værdierne er sat. Her er værdierne sat random men stadig med min kontrol. I den forbindelse vil der opstå en form for naturlighed i mit program , da man ikke ved første øjekast kan se præcis hvordan de er placeret. Jeg har ligeledes ved at ,i mit for-loop, bede den genere 8 rektangler men ikke fortælle den de eksakte placeringer, skabt den naturlighed der  fremkommer i en generativ kode og i generativ kunst. I den forbindelse kunne man også implementere begrebet 'conditional branching' som ligeledes underbygge dette med random og naturlighed. 

## Reference

https://p5js.org/reference/

Soon, Winnie & Cox, Geoff - Aesthetic programming, A handbook of Software Studies

