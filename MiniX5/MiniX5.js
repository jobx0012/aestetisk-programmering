
// Jeg vil gerne have nogle globale variabler der giver mig frihed nede i mit forloop. Dette gør at min kode kan ændres på en mere fleksibel måde. 
let firkant1x 
let firkant1y 

let firkant2x 
let firkant2y

let firkant3x
let firkant3y

let firkant4x
let firkant4y

let firkant5x
let firkant5y

let firkant6x
let firkant6y

let firkant1brede = 100
let firkant1højde = 100

let firkant1brede1 = 100
let firkant1højde1 = 100 

let firkant1brede2 = 100
let firkant1højde2 = 100

let firkant1brede3 = 100
let firkant1højde3 = 100

let firkant1brede4 = 100
let firkant1højde4 = 100 

let firkant1brede5 = 100
let firkant1højde5 = 100 

let a1 = 100
let a2 = 100 
let a3 = 100 
let a4 = 100 
let a5 = 100 
let a6 = 100 

function setup(){
createCanvas(windowWidth,windowHeight);

firkant1x = windowWidth/2
firkant1y = windowHeight/2


firkant2x = windowWidth/2-325
firkant2y = windowWidth/2-325


firkant3x = windowWidth/2+325
firkant3y = windowWidth/2-325


firkant4x = windowWidth/2+625
firkant4y = windowWidth/2-325


firkant5x = windowWidth/2-625
firkant5y = windowWidth/2-325

firkant6x = windowWidth/2-925
firkant6y = windowWidth/2-325


}

function draw (){
  background (255,200,200);
  //stroke(random(0,255),random(0,255),random(0,255))
  stroke(220,20,60);
  frameRate();
  strokeWeight(2);
  noFill() 
  rectMode(CENTER);


  for (let tæller = 0; tæller < 8; tæller++){
  
    const afstand = 155 -(tæller *30);
  
    rect(firkant1x,firkant1y,firkant1brede,firkant1højde);
  
    rect(firkant1x,firkant1y-((tæller+1)*afstand),firkant1brede-((tæller+1)*30), firkant1højde-((tæller+1)*30));
    rect(firkant1x, firkant1y+((tæller+1)*afstand),firkant1brede-((tæller+1)*30), firkant1højde-((tæller+1)*30));

    firkant1x = firkant1x + Math.sin(a1);

    a1 +=random(0.005)

    if(rect >= 1500){
      rect = 50;
 }

  }

  for(let tæller = 0; tæller < 8; tæller++){

    const afstand1 = 155-(tæller*30);

    rect(firkant2x,firkant2y,firkant1brede1,firkant1højde1);

    rect(firkant2x,firkant2y-((tæller+1)*afstand1),firkant1brede1-((tæller+1)*30),firkant1højde1-((tæller+1)*30));
    rect(firkant2x,firkant2y+((tæller+1)*afstand1),firkant1brede1-((tæller+1)*30),firkant1højde1-((tæller+1)*30));

    firkant2x = firkant2x + Math.sin(a2);

    a2 += random(0.002)

    if(firkant2x>=1500){
      firkant2x = 50;
 }


  }

  for(let tæller = 0; tæller < 8; tæller++){

    const afstand2 = 155-(tæller*30);

    rect(firkant3x,firkant3y,firkant1brede2,firkant1højde2);

    rect(firkant3x,firkant3y-((tæller+1)*afstand2),firkant1brede2-((tæller+1)*30),firkant1højde2-((tæller+1)*30));
    rect(firkant3x,firkant3y+((tæller+1)*afstand2),firkant1brede2-((tæller+1)*30),firkant1højde2-((tæller+1)*30));

    firkant3x = firkant3x + Math.sin(a3);

    a3 += random(0.005)

    if(firkant3x>=1500){
      firkant3x = 50;
 }


  }

  for(let tæller = 0; tæller < 8; tæller++){

    const afstand3 = 155-(tæller*30);

    rect(firkant4x,firkant4y,firkant1brede3,firkant1højde3);

    rect(firkant4x,firkant4y-((tæller+1)*afstand3),firkant1brede3-((tæller+1)*30),firkant1højde3-((tæller+1)*30));
    rect(firkant4x,firkant4y+((tæller+1)*afstand3),firkant1brede3-((tæller+1)*30),firkant1højde3-((tæller+1)*30));


    firkant4x = firkant4x + Math.sin(a4);
  
    a4 += random(0.002)

}

for(let tæller = 0; tæller < 8; tæller++){

  const afstand4 = 155-(tæller*30);

  rect(firkant5x,firkant5y,firkant1brede4,firkant1højde4);

  rect(firkant5x,firkant5y-((tæller+1)*afstand4),firkant1brede4-((tæller+1)*30),firkant1højde4-((tæller+1)*30));
  rect(firkant5x,firkant5y+((tæller+1)*afstand4),firkant1brede4-((tæller+1)*30),firkant1højde4-((tæller+1)*30));

  firkant5x = firkant5x + Math.sin(a5);
  

  a5 += random(0.005)

  if(firkant5x>=1500){
    firkant5x = 50;
}

}

for(let tæller = 0; tæller < 8; tæller++){

  const afstand5 = 155-(tæller*30);

  rect(firkant6x,firkant6y,firkant1brede5,firkant1højde5);

  rect(firkant6x,firkant6y-((tæller+1)*afstand5),firkant1brede5-((tæller+1)*30),firkant1højde5-((tæller+1)*30));
  rect(firkant6x,firkant6y+((tæller+1)*afstand5),firkant1brede5-((tæller+1)*30),firkant1højde5-((tæller+1)*30));

  firkant6x = firkant6x + Math.sin(a6);

  a6 += random(0.002)

  /*if(firkant6x>=1500){
    firkant6x = 50;
}*/

}





}


    //a = a + minInc;


//let minInc = 2

//rect(tæller * firkant2x, firkant2y, tæller * firkant2x, firkant2y + sin(a) * firkant2x);


//rect(firkant2x,firkant2y,firkant1brede1,firkant1højde1);
  /*for(let tæller = 0; tæller <5; tæller ++){

  const afstand1 = 155-(tæller*20);
  rect(firkant2x,firkant2y-((tæller+1)*afstand1),firkant1brede1-((tæller+1)*20),firkant1højde1-((tæller+1)*20));
  rect(firkant2x,firkant2y+((tæller+1)*afstand1),firkant1brede1-((tæller+1)*20),firkant1højde1-((tæller+1)*20));

  firkant2x = windowWidth/2-325
  firkant2y = windowWidth/2-325

  rect(tæller * firkant2x, firkant2y, tæller * firkant2x, firkant2y + sin(a) * firkant2x);
  a = a + minInc;

  }*/

  /*for(let tæller = 0;tæller<5; tæller ++){
    const afstand2 = 155-(tæller*20);
    rect(firkant3x,firkant3y-((tæller+1)*afstand2),firkant1brede2-((tæller+1)*20),firkant1højde2-((tæller+1)*20));
    rect(firkant3x,firkant3y+((tæller+1)*afstand2),firkant1brede2-((tæller+1)*20),firkant1højde2-((tæller+1)*20));

    firkant3x = windowWidth/2+225
    firkant3y = windowWidth/2+225}*/



