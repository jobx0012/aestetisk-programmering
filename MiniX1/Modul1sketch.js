//Bevægelse af grøn figur
let y=0;
let y1=-30;
let y2=-30;
let y3=40;
let y28=5;
let y29=5;

//Bevægelse af lyserød figur
let y4=0;
let y5=-30;
let y6=-30;
let y7=40;
let y26=5;
let y27=5;

//Bevæglese af blå figur
let y8=0;
let y9=-30;
let y10=-30;
let y11=40;
let y24=5;
let y25=5;

//Bevæglese af gul figur 
let y12=0;
let y13=-30;
let y14=-30;
let y15=30;
let y22=5;
let y23=5;

//Bevægelse af Lilla figur
let y16=0;
let y17=-30;
let y18=-30;
let y19=40;
let y20=5;
let y21=5;


function setup() {
  // Størrelsen på canvas
  createCanvas(windowWidth, windowHeight);

}

function draw(){
  // Grundbaggrund 
  background(255,153,204);
  strokeWeight(0.5);
  stroke(0);

 //Stribet baggrund
  fill(255,255,255);
  var x =0;

  rect(x,0, 1600, 25);
  x = x + 0;
  rect(x,50, 1600, 25);
  x = x + 0;
  rect(x,100, 1600, 25);
  x = x + 0;
  rect(x,150, 1600, 25);
  x = x + 0;
  rect(x,200, 1600, 25);
  x = x + 0;
  rect(x,250, 1600, 25);
  x = x + 0;
  rect(x,300, 1600, 25);
  x = x + 0;
  rect(x,350, 1600, 25);
  x = x + 0;
  rect(x,400, 1600, 25);
  x = x + 0;
  rect(x,450, 1600, 25);
  x = x + 0;
  rect(x,500, 1600, 25);
  x = x + 0;
  rect(x,550, 1600, 25);
  x = x + 0;
  rect(x,600, 1600, 25);
  x = x + 0;
  rect(x,650, 1600, 25);
  x = x + 0;
  rect(x,700, 1600, 25);
  x = x + 0;
  rect(x,750, 1600, 25);
  x = x + 0;
  rect(x,800, 1600, 25);
  x = x + 0;
  rect(x,850, 1600, 25);
  x = x + 0;
  rect(x,900, 1600, 25);
  x = x + 0;



  //BLÅ SMILEY 1
  fill(0,204,255);
  ellipse(150,y,175,175);
  y++;
  fill(255,255,255)
  rect(200,y1,20,40,2);
  y1++;
  rect(173,y2,20,40,2);
  y2++;
  arc(200,y3,50,40,0,PI);
  y3++;
  fill(0,0,0);
  circle(215,y28,12);
  y28++;
  circle(190,y29,12);
  y29++;

  //BLÅ SMILEY 2
  fill(0,153,255);
  ellipse(450,y8,175,175);
  y8++;
  fill(255,255,255);
  rect(500,y9,20,40,1.5);
  y9++;
  describe('white rect with black outline and round edges in mid-right of canvas');
  rect(473,y10,20,40,1.5);
  y10++;
  describe('white rect with black outline and round edges in mid-right of canvas');
  fill(255,255,255);
  arc(500,y11,50,40,0,PI);
  y11++;
  fill(0,0,0);
  circle(515,y26,12);
  y26++;
  circle(490,y27,12);
  y27++;

  //BLÅ SMILEY 3
  fill(0,102,255);
  ellipse(750,y12,175,175);
  y12++;
  fill(255,255,255);
  rect(750,y13,20,40,2);
  y13++;
  rect(725,y14,20,40,2);
  y14++;
  arc(750,y15,50,40,0,PI);
  y15++;
  fill(0,0,0);
  circle(760,y24,12);
  y24++;
  circle(735,y25,12);
  y25++;

  //BLÅ SMILEY 4 
  fill(0,51,255);
  ellipse(1050,y4,175,175);
  y4++;
  fill(255,255,255);
  rect(1000,y5,20,40,1.5);
  y5++;
  describe('white rect with black outline and round edges in mid-right of canvas');
  rect(975,y6,20,40,1.5);
  y6++;
  describe('white rect with black outline and round edges in mid-right of canvas');
  arc(1000,y7,50,40,0,PI);
  y7++;
  fill(0,0,0);
  circle(980,y22,12);
  y22++;
  circle(1001,y23,12);
  y23++;
  
  //BLÅ SMILEY 5 
  fill(0,0,255);
  ellipse(1350,y16,175,175);
  y16++;
  fill(255,255,255);
  rect(1275,y17,20,40,1.5);
  y17++;
  rect(1300,y18,20,40,1.5);
  y18++;
  arc(1300,y19,50,40,0,PI);
  y19++;
  fill(0,0,0)
  circle(1279,y20, 12);
  y20++;
  circle(1302,y21,12);
  y21++;



}