# MiniX 8 - Gruppe 5, Maise, Mads, Job, Amalie 


## Flowchart 1 

Platform (skærmen på computer) Ukoncentration -  med tekst på, hvor der er information vedrørende hvor meget apps påvirker vores koncentrationsevne (bred målgruppe). Rundt om denne informationstekst, skal der dukke flere distraherende emner op. Dette kunne være farver, billeder, lyd, video. Dette ville vi bruge random() funktionen til, eller noise(). Tilføje en eye tracking hvor vi vil understrege hvor din koncentration er rykket hen til andre emner. Her skal der også være en counter der tæller hvor længe du har læst ved hvert emne der er dukket op. 

![](/MiniX8/Flowchart_1.png)


## Flowchart 2 
Telefon-platform - det nye sunde sociale medie. Vi kan ikke forbyde sociale medier, så vi vil danne igennem underholdningen. Der dukker en (ikke nødvendigvis) spændende fact op efter du har scollede ned efter et hvis antal videoer, hvor du herefter skal svare på et spørgsmål der omhandler den fact/tekst du netop har læst. Du kan altså ikke komme videre, før du har svaret på spørgsmålet. Dette skal dermed være en måde at få børn til at få viden samtidig med de ser Tik Tok, og vi dermed undgår at ungdommen bruger mange timer på Tik Tok.

![](/MiniX8/Flowchart_2.png)


**Hvilke vanskeligheder er der forbundet med at forsøge at holde tingene enkle på kommunikationsniveau og samtidig opretholde kompleksiteten på det algoritmiske procedure niveau?**

Idet vi har lavet et flowchart imødekommer vi en bredere gruppe, i og med vi visualiserer fremgangsmåden, hvorpå programmet opererer og bliver sat op. Det forklarer også på en meget overskuelig og tilgængelig måde, hvordan og hvorfra den tager beslutninger, og hvad udfaldet vil blive.

Hvis de derimod sad med koden til programmet, ville de ikke forstå, hvad der skete hvor i programmet, idét det kan blive meget uoverskueligt at se og forstå helheden i koden, når det bare står i en lang smørre. Derfor kan med fordel programmører ty til flowcharts for at det bliver mere overskueligt for brugeren og for dem selv. Det er også en måde at pseudo kode, så det er en god måde for dem selv at få en dybere forståelse for, hvad syntakserne gør.

Vanskelighederne sker, idet vi som programmører skal oversætte og kommunikerer koden ud til menneskesprog, idet kodningen er computersprog. Programmering og kodning er computersprog, hvilket er meget sort og hvidt. Computeren skal have skåret alt ud i pap, og der er mange regler, man skal tage højde for i programmering, som man ikke nødvendigvis skal tage højde for i menneskesprog. Derfor kan det være svært at oversætte programmering til et flowchart, idet det hurtigt kan virke meget banalt og selvsagt i menneskesprog, men det er det ikke på computersprog på baggrund af alle de førnævnte regler. 



**Hvad er de tekniske udfordringer, som de to idéer står over for, og hvordan vil De tackle dem?**

Ide 1 står overfor problemer når det kommer til de forskellige objekter, som bliver tilføjet herunder eye-tracking, lyd og randomness. Alle disse objekter skal fungere i programmet uden at påvirke hinanden. Dette kan skabe en del problematikker, da programmet vil have svært ved at køre optimalt.
 
Et andet problem kan være at gøre teksten, som vi gerne vil have at brugeren læser, spændende. Brugeren vil derfor hurtigere og nemmere blive distraheret af de forstyrrende objekter. Dvs. brugerne skal bliver draget af teksten, for hvis brugeren ikke bliver draget af teksten, så falder hele idéen til jorden. 
Et tredje problem kan også være, hvis teksten er for dragende og de distraherende objekter ikke er. Dette er ikke et problem vi regner med vil opstå, men kan dog stadig være en faktor.

Et fjerde problem ved ide 1 er, at for at vi kan bruge eyetracking og give budskabet på den sidste side, så skal brugeren give os lov til at bruge deres webcam, så vi kan tracke. Ellers kan det være svært at forstå helheden ved idéen, idet man ikke ser, hvor lang tid ens øjne egentlig vandrer væk fra teksten.

Idé 2 står overfor et problem med at den er koblet op på Tik Tok. Det kunne være så fedt hvis den faktisk virkede, f.eks. igennem apple automation, eller hvis vi med iframe eller en anden javascript function kunne få det til at fungere med skift mellem TikTok, og "sund-mobil-" appen.

Programmet skal altså fungere, men det skal også gerne kunne gøre en forskel og faktisk gøre mobilbrug sundere og bedre for børn. Så vi skal finde en god balance mellem at gøre det spændende, men ikke for spændende, for vi ser helst at børnene helt dropper at bruge mobilen.


**På hvilke måder er de individuelle og gruppe diagrammer, du producerede, nyttige?**

De er nyttige fordi de skaber et overblik over hvordan vi skal/har programmeret vores programmer/program. Vi skiller vores program ad, og ser på hvad hvert element gør for programmet, på den måde kan vi se hvad vært input giver for et output. Der giver os som programmører også et overblik over rækkefølgen på programmet, hvor i programmet det sker og hvilke valg, det reelt skal tage.

