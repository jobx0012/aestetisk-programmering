# ReadMe – MiniX3 

#### Introduktion & temporalitet
I denne uges MiniX3 opgave skulle vi lave en Throbber. Før vi lærte hvad en Throbber havde af mening, var jeg overbevist om at det var noget computeren havde genereret, og ikke noget som mennesker genere for at informere andre mennesker om at computeren tænker. Det vil sige at det ikke er for computerens skyld, men os mennesker der sidder og er utålmodige når computeren ikke gør som vi ønsker. Jeg er selv utrolig utålmodig og bliver ofte irriteret over når min computer er langsom. Jeg har derfor i denne opgave valgte at udfordre min egen følelse når det handler om mine tanker omkring temporalitet. Min måde at tolke den menneskelige følelse er at lave et design hvor man bliver glad af at kigge på den, og kan se på den længe grundet den bevægelse og æstetiske udtryk. Jeg har yderligere også haft ordet temporalitet meget med ind over når jeg har designet min throbber. Hvis man reflektere over temporalitet i forholdt til computeren så er der stor forskel. Jeg synes det er spændende at dykke ned i hvordan computerens opfattelse af tid er versus menneskets opfattelse af tid er. Jeg tror ikke at man til dagligt tænker over at temporalitet har så meget at sige i forholdt til computeren. Det kunne derfor være interessant at udfordre computerens opfattelse af tid og gøre den mere menneskelig. 

![](/MiniX3/Skærmbillede_miniX3.png)

**Link til mit program:** https://sofieamaliem.gitlab.io/aestetisk-programmering/MiniX3/index.html


**Link til min kode:** https://gitlab.com/sofieamaliem/aestetisk-programmering/-/blob/main/MiniX3/MiniX3.js

# Min kode / Throbber 

Jeg har i denne kode udfordrede min egen forståelse af programmering. Jeg har derfor brugt både variabler og if-statements i denne opgave. Jeg vil dermed fokusere på de mere på de krævende koder og gå hurtigere over de efterhånden ’basale’. I min forklaring af min kode begynder jeg med baggrundene og dernæst forklarer mine globale og lokale variabler, funktioner og if-statements. 

Min kode er en fire baggrunde og lag oven på hinanden for at skabe den rigtige effekt. Jeg er derfor begyndt med en baggrund i lyseblå, den har jeg placeret i starten af min kode (referer til linje 26).

`background (204,255,255);`

Dernæst har jeg sart min blå baggrund på som er baggrunden på den lille computerskærm og samtidig brugt 
`rectMode (CENTER); `og placeret mit rektangel hvor den skulle være `fill (0,191,255); / rect (800,400, 600, 500);` _(referer til linje 30-32)_.

For at få mine ænder til at være bag vandet, har jeg placeret dette i _linje 62-63_ og brugt `rect (800,570,600,300)` så den passede med den anden rect jeg har brugt. 

For at mine ænder ikke var synlige når de kørte ud af billedet, har jeg sat to rektangler på hver side af billedet i samme farve som baggrunden.  

Dernæst for at placere min computerskærms-kanter, har jeg først lavet dem hvide og dernæst lavede hver kant enkeltvis, så de havde samme højde som den blå baggrund og så ændrede på min x og y-værdier alt efter om de skulle placeres vandret, eller lodret på mit canvas _(referer til linje 70-79 i min kode):_

```
//Computerskærm kanter 
fill (255);

//Venstre kant & toppen
rect (499,435, 25, 580,10);
rect (800,150, 625, 25,10);

//højre kant & bunden
rect (1099,435, 25, 580,10);
rect (800,725, 625, 50);
```

# Variabler, funktioner og if-statements

Efter at mine baggrunde er placeret, skal mine ænder nu tilføjes. Da de skal være bag ved vandet, er de lavet på linje 35-58 i min kode. Da jeg har flere ænder, lavede jeg en funktion for de tre lyse ænder. Min funktion er placeret uden for draw og setup, da det er en selvstændig funktion: 

```
function makeAnd(x,y){ 

//Krop 
fill (255,246,143);
noStroke();
ellipse (x, y,120,75);
ellipse(x,y-50,50,50);
    
//næb
fill(255,69,0);
ellipse(x+25,y-50,20,6); 

//øje yderste 
fill(0);
ellipse(x+10,y-55,8,8); 

//øje nederste 
fill(255);
ellipse(x+12,y-55,3,3);
```


Jeg har tegnet mine ænder ved hjælp af ellipser. Jeg vil forklare ud fra denne linje kode, hvordan jeg har gjort: 

`ellipse (x, y,120,75);`

Denne overstående ellipse er grundformen på min and. Placering af denne ellipse er `(600,400) `som er min `x` og `y-værdi`. Jeg har derfor skiftet `600` og `400` ud med `x` og `y` i min funktion, så jeg kan bruge min funktion oppe i min kode, og på den måde ikke sidde og hard-code mine ænder. For at placeringen er korret, lægger jeg enten den værdi til min `x værdi` eller trækker den fra min `x-værdi`. 

**Se eksempel nedenfor:**

`ellipse(x,y-50,50,50);`

Dette har jeg gjort ved hele min and så værdierne stemte overens med den ønskede placering. 

Jeg brugte samme fremgangsmåde, da jeg skulle lave mine mørke ænder, jeg ændrede blot min `x` og `y-værdier` til at være `x1` og `y2` da de ikke skulle placeres forskudt af de lyse ænder. 

For at være sikker på at mit program ved hvad mine `x-og y-værdier` er, har jeg lavet lokale variabler ved at sige at _(referer til linje 16-19 i min kode)_:

```
x=600;
y=400;
x1=600;
y2=400;
```


Jeg bruger min funktion i min `function draw()` og placere den i `push()` og `pop()` som jeg bruger for at gemme henholdsvis den aktuelle stil og gendanne indstilling.

Inde i min `push()` og `pop()` skriver jeg følgende _(referer til linje 35-41 i min kode)_:

```
push();
makeAnd(x,y);
makeAnd(x+200,y);
makeAnd(x+400,y);

x = x + speed1;
pop();
```


Jeg kan, fordi jeg har lavet min funktion der hedder `’makeAnd’` blot lægge `hhv. 200 og 400` til mine `x og y-værdier` placerer mine ænder forskudt af hinanden. 

Jeg skal ligeledes have mine ænder til at bevæge sig for at understøtte min throbber effekt, jeg skriver derfor `x = x + speed1;`

Min `speed1` er en global variabel jeg har defineret til at være på `5`. Det vil altså sige at ved at skrive overstående, så er min `x-værdi` lig med min `x-værdi` plus en `speed på 5` og dermed skaber jeg en bevægelse. 

Udenfor min `push()` og `pop()` funktion har jeg gjort brug af et `if-statement `så jeg kan styre hvornår mine ænder skal stoppe og starte igen. 

```
if(x>=1400){
     x = 50;
}
```

For lige at forklare mit if-statement, så har jeg sagt at `hvis x er større end (>) eller lig med (=) 1400 pixels på mit canvas så skal x = 50`. Så altså når mine ænder er nået til 1400 pixels på mit canvas, så skal de starte på 50 pixels på mit canvas. 

Jeg har gjort brug af samme metode til at definere mine globale variabler, funktioner og if-statements i mine mørke ænder `(drawAnd)`

```
push();
drawAnd(x1-50,y);
drawAnd(x1+150,y2);
drawAnd(x1+350,y2);

pop();

x1 = x1 + speed2;

if(x1>=1400){
     x1 = 50;
}
```


## Reference
https://p5js.org/reference/

Soon, Winnie & Cox, Geoff - Aesthetic programming, A handbook of Software Studies

